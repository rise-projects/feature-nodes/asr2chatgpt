# ASR2ChatGPT

This project connects Automatic Speech Recognition (ASR) with a large language model to generate responses based on spoken requests. In this project, we employ the [Whisper](https://github.com/openai/whisper) ASR in conjunction with the [GPT4All](https://gpt4all.io/index.html) project. The graphic below offers a conceptual overview into how the components communicate with each other.

<img src="https://gitlab.ub.uni-bielefeld.de/rise-projects/rise-documentation/-/raw/development/source/images/RISE_ASR2GPT.jpg?ref_type=heads" width="800" />

## Install requirements

```
sudo apt-get install python3-tk
sudo apt install ffmpeg
sudo apt install python3-pyaudio
sudo apt-get install portaudio19-dev

sudo pip install --upgrade numpy
```

## Install Whisper AI & GPT4All

```
recommentation: using venv:
python3 -m venv .venv
source .venv/bin/activate

pip install -r requirements.txt

```

## Model

Change directory to the gpt4all cache folder (generally in `home/.cache/gpt4all`) 
and download the external models:

```
mkdir ~/.cache/gpt4all
cd ~/.cache/gpt4all

# Download wizard model as primary model for gpt4all
wget -nc https://gpt4all.io/models/wizardlm-13b-v1.1-superhot-8k.ggmlv3.q4_0.bin

# Download alternative model falcon for testing
wget -nc https://huggingface.co/nomic-ai/gpt4all-falcon-ggml/resolve/main/ggml-model-gpt4all-falcon-q4_0.bin
```

## Test if everything is working

If no errors appear, the installation was successful.

``` python
from gpt4all import GPT4All
model = GPT4All("wizardlm-13b-v1.1-superhot-8k.ggmlv3.q4_0.bin")
```

## Side Note:
https://github.com/openai/whisper/discussions/63
Source of Links direclty to the whisper models, nesessary for the Jenkins file