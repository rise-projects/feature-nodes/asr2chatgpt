import gpt4all
import rospy
from rise_core.msg import DomainTaskActionGoal
from rise_core.srv import WorkingMemoryService
from std_msgs.msg import String
import json
import csv
import os

directoryLocation = os.path.dirname(os.path.realpath(__file__))

def gpt_generating_answer():
    question = get_value("H_asr_request")
    print("Request:      " + question)
    # 50 2 (ok answer but somteimes too short)
    answer = gpt.generate(prompt=question, temp=0.16, max_tokens=200)
    print("Response:     " + answer)

    assign_value("R_gpt_response", answer, True)
    raise_event_topic("/FN_gpt_finished_processing")


def assign_value(key, value, is_string):
    assign_val = DomainTaskActionGoal()
    assign_val.goal.function_type = "assignValue"
    if is_string:
        assign_val.goal.domain_task_behavior = '{"' + key + '" : \'' + value + '\'}'
    else:
        assign_val.goal.domain_task_behavior = '{"' + key + '" : ' + value + '}'

    pub.publish(assign_val)


def raise_event_topic(name):
    raise_event_top = DomainTaskActionGoal()
    raise_event_top.goal.function_type = "raiseEventTopic"
    raise_event_top.goal.domain_task_behavior = name

    pub.publish(raise_event_top)


def start_cra(name):
    raise_event_top = DomainTaskActionGoal()
    raise_event_top.goal.function_type = "startCommunicationRobotAct"
    raise_event_top.goal.domain_task_behavior = name

    pub.publish(raise_event_top)


def get_value(key):
    rospy.wait_for_service('/rise/set/memoryService')
    try:
        memory_service = rospy.ServiceProxy('/rise/set/memoryService', WorkingMemoryService)
        resp1 = memory_service(key)
        pair = json.loads(resp1.json_response)
        return pair[key]
    except rospy.ServiceException as e:
        print("Service call failed: %s" % e)


def rise_state_callback(data):
    rise_state_event = json.loads(data.data)["transition_event"]
    if(rise_state_event == "/FN_asr_finished_processing"):
        print('Generating response...')
        gpt_generating_answer()


def read_system_template():
    csvFileLocation = directoryLocation+"/system_template.csv"
    data = []
    with open(directoryLocation+"/system_template.csv", 'r') as csvfile: 
        reader = csv.reader(csvfile, skipinitialspace=True)
        data.append(tuple(next(reader)))
        for key, val in reader:
            data.append((key, val))

    print(str(data))
    return str(data)



    with open(csvFileLocation, "r") as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            if row["key"] == "system_template":
                return row["value"]
    return None


if __name__ == '__main__':
    try:
        pub = rospy.Publisher('/rise/set/domainTask/goal', DomainTaskActionGoal, queue_size=3)

        rospy.Subscriber('/rise/state/update', String, rise_state_callback)

        rospy.init_node('GPT4AllFeatureNode', anonymous=True)
        gpt = gpt4all.GPT4All("wizardlm-13b-v1.1-superhot-8k.ggmlv3.q4_0.bin","/vol/distribution/share/asr2chatgpt" ,n_threads=12)

        print('Initializing GPT session...')
        
        system_template = read_system_template()
        if system_template is None:
            print("Error: Could not read system_template from CSV file.")
            exit(1)
        
        with gpt.chat_session(system_template):
            print('GPT is waiting for request...')

            answer = gpt.generate(prompt="Mach dich bereit für die Eröffnung.", temp=0.16, max_tokens=200)
            start_cra("R_System_Ready")

            rospy.spin()

    except rospy.ROSInterruptException:
        pass
