import whisper
import pyaudio
import wave
import tty, sys, termios
import rospy
from rise_core.msg import DomainTaskActionGoal
from rise_core.srv import WorkingMemoryService
from std_msgs.msg import Bool
import threading


class WhisperToRise:
    active_state = False
    model = None
    p = None
    t1 = None

    def is_recording(self):
        
        stream = self.p.open(format=self.sample_format,
                        channels=self.channels,
                        rate=self.fs,
                        frames_per_buffer=self.chunk,
                        input=True)
        frames = []  # Initialize array to store frames

        while True:
            for i in range(0, int(self.fs / self.chunk * self.seconds)):
                data = stream.read(self.chunk)
                frames.append(data)
            if not self.active_state:
                break
        stream.stop_stream()
        stream.close()

        print("Finished recording")

        # Save the recorded data as a WAV file
        wf = wave.open(self.filename, 'wb')
        wf.setnchannels(self.channels)
        wf.setsampwidth(self.p.get_sample_size(self.sample_format))
        wf.setframerate(self.fs)
        wf.writeframes(b''.join(frames))
        wf.close()

        print(self.filename)
        result = self.model.transcribe(audio=self.filename, initial_prompt='Floka')
        text = result["text"]
        if text != '' and not rospy.is_shutdown():
            self.publish_result(text)


    def publish_result(self,text):
        print("Human request:   " + text)
        self.assign_value("H_asr_request", text, True)

        self.raise_event_topic("/FN_asr_finished_processing")


    def assign_value(self,key, value, is_string):
        assign_val = DomainTaskActionGoal()
        assign_val.goal.function_type = "assignValue"
        if is_string:
            assign_val.goal.domain_task_behavior = '{"' + key + '" : \'' + value + '\'}'
        else:
            assign_val.goal.domain_task_behavior = '{"' + key + '" : ' + value + '}'

        self.pub.publish(assign_val)


    def raise_event_topic(self,name):
        raise_event_top = DomainTaskActionGoal()
        raise_event_top.goal.function_type = "raiseEventTopic"
        raise_event_top.goal.domain_task_behavior = name

        self.pub.publish(raise_event_top)


    def asr_listener_callback(self,data):
        self.active_state = data.data

        if(self.active_state):
            self.t1 = threading.Thread(target=self.is_recording)
            self.t1.start()

    def __init__(self):
        try:
            #self.orig_settings = termios.tcgetattr(sys.stdin)
            #tty.setcbreak(sys.stdin)

            self.chunk = 1024  # Record in chunks of 1024 samples
            self.sample_format = pyaudio.paInt16  # 16 bits per sample
            self.channels = 2
            self.fs = 44100  # Record at 44100 samples per second
            self.seconds = 3
            self.filename = "question.wav"

            self.pub = rospy.Publisher('/rise/set/domainTask/goal', DomainTaskActionGoal, queue_size=3)
            rospy.init_node('ASRFeatureNode', anonymous=True)

            print('Initializing model..')
            self.model = whisper.load_model("small", download_root=".")

            # Create an interface to PortAudio
            self.p = pyaudio.PyAudio()

            self.whisperStateSubscriber = rospy.Subscriber('/asr/state/recording', Bool, self.asr_listener_callback)
            print("ASR is ready..")

            rospy.spin()

        except rospy.ROSInterruptException:
            pass

if __name__ == "__main__":
    app = WhisperToRise()