import rospy
import json
import time
from rise_core.msg import DomainTaskActionGoal
from rise_core.srv import WorkingMemoryService
from geometry_msgs.msg import Vector3

norm_x = None
norm_y = None
current_time = None
last_publish_time = None
update_time = None

max_x=None
max_y=None
min_x=None
min_y=None

shift_x=None
shift_y=None


def assign_value(key, value):
    assign_val = DomainTaskActionGoal()
    assign_val.goal.function_type = "assignValue"
    assign_val.goal.domain_task_behavior = '{"' + key + '" : ' + str(value) + '}'

    pub.publish(assign_val)

def get_value(key):
    rospy.wait_for_service('/rise/set/memoryService')
    try:
        memory_service = rospy.ServiceProxy('/rise/set/memoryService', WorkingMemoryService)
        resp1 = memory_service(key)
        pair = json.loads(resp1.json_response)
        return pair[key]
    except rospy.ServiceException as e:
        print("Service call failed: %s" % e)

def getCalibationParameter():
    global norm_y, norm_x, current_time, update_time, last_publish_time, min_x, min_y, max_x, max_y ,shift_x, shift_y

    max_x = get_value("max_x")
    max_y = get_value("max_y")
    min_x = get_value("min_x")
    min_y = get_value("min_y")

    shift_x = get_value("shift_x")
    shift_y = get_value("shift_y")

    norm_x = get_value("S_x_norm")
    norm_y = get_value("S_y_norm")
    update_time = get_value("gaze_update_interval")

def gaze_to_rise_callback(data):

    global norm_y, norm_x, current_time, update_time, last_publish_time, min_x, min_y, max_x, max_y ,shift_x, shift_y
    
    
    current_time = time.time()
    if(current_time - last_publish_time > update_time):

        x = data.x
        y = data.y
        print("X" +str(x))
        print("Y"+str(y))
        x = x + shift_x
        y = y + shift_y


        if(x < min_x):
            x = min_x
        elif(x > max_x):
            x = max_x

        if(y < min_y):
            y = min_y
        elif(y > max_y):
            y = max_y



        translated_pos_x = x / norm_x 
        translated_pos_y = -1 * (y / norm_y) 
        assign_value("H_x_pos", translated_pos_y)
        assign_value("H_y_pos", translated_pos_x)
        assign_value("H_z_pos", 1)
        last_publish_time = time.time()

if __name__ == "__main__":
    try:
        rospy.init_node('Openface2RISE', anonymous=True)

        current_time = time.time()
        last_publish_time = time.time()

        pub = rospy.Publisher('/rise/set/domainTask/goal', DomainTaskActionGoal, queue_size=3)



        getCalibationParameter()


        rospy.Subscriber('/openface/pose/location', Vector3, gaze_to_rise_callback)

        rospy.spin()
    except rospy.ROSInterruptException:
        pass