#!/usr/bin/python3
import tkinter as tk
from tkinter import *
import rospy
from std_msgs.msg import Bool
from std_msgs.msg import String
from rise_core.srv import WorkingMemoryService
import json


from tkinter.scrolledtext import ScrolledText

class IoGuiApp:
    asrIsActiv = False
    recordingNotActivColor="#d9fcd9"
    recordingActivColor="#d926ff"
    isGeneratingColor="#D3D3D3"
    updateInputFieldIRState="/FN_asr_finished_processing"
    updateOutputFieldIRState="/FN_gpt_finished_processing"
    waitForRequestIRState="WaitingForRequest"

    def __init__(self, name, master=None):
        # Initialize ROS node
        rospy.init_node(name, anonymous=True)
        rospy.loginfo("ROS-Node " + name + " initialize..")

        #publisher
        self.asrControllingPublisher = rospy.Publisher('/asr/state/recording', Bool, queue_size = 10)

        #Listener
        self.listener()

        self.root = tk.Tk()
        self.root.resizable(False,False)
        self.root.title("Virtual Agent Floka - Input/Output Stream")

        # build ui
        self.TopLevel = tk.Frame(self.root)
        self.TopLevel.configure(height=800, width=800)

        frame2 = tk.Frame(self.TopLevel)
        frame2.configure(height=200, width=200)
        label1 = tk.Label(frame2)
        label1.configure(font="{Ubuntu} 18 {}", text='Input Stream:')
        label1.pack(anchor="w", side="top")
        self.inputTextField = tk.Text(frame2)
        self.inputTextField.configure(
            font="{Ubuntu} 20 {}",
            height=3,
            setgrid=True,
            width=50,
            state='disabled')
        self.inputTextField.pack(expand=True, fill="both", side="top")
        frame2.pack(anchor="nw", expand=True, fill="x", side="top")
        frame21 = tk.Frame(self.TopLevel)
        frame21.configure(height=200, width=200)
        label5 = tk.Label(frame21)
        label5.configure(font="{Ubuntu} 18 {}", text='Output Stream:')
        label5.pack(anchor="w", side="top")

      

        self.outputTextField = ScrolledText(
    master = frame21,
    wrap   = tk.WORD,
    width  = 50,
    height = 10
)
        self.outputTextField.configure(font="{Ubuntu} 20 {}", height=5, width=50, state='disabled')
        self.outputTextField.pack(expand=True, fill="both", side="top")
        frame21.pack(anchor="nw", expand=True, fill="x", side="top")
        #self.resetButton = tk.Button(self.TopLevel)
        #self.resetButton.configure(
        #    background="#d92633",
        #    activebackground="#d92633",
        #    font="{Ubuntu} 12 {}",
        #    height=3,
        #    text='Reset\nHistory',
        #    width=10,
        #    command=self.resetButton_Pressed)
        #self.resetButton.pack(side="right")
        self.pushToTalkButton = tk.Button(self.TopLevel)
        self.pushToTalkButton.configure(
            background=self.recordingNotActivColor,
            activebackground=self.recordingNotActivColor,
            font="{Ubuntu} 20 {}",
            height=2,
            text='Start Listenting',
            width=70,
            command=self.pushToTalkButton_Pressed)
        self.pushToTalkButton.pack(expand=True, side="bottom")
        self.TopLevel.pack(expand=True, fill="both", side="top")

        # Main widget
        self.mainwindow = self.TopLevel

    def run(self):
        self.mainwindow.mainloop()
        print("unregister subscriber")

        self.asrControllingPublisher.unregister()

        print("closing")


    def listener(self):
        self.riseStateSubscriber = rospy.Subscriber("/rise/state/update",String, self.riseStateCallback)

    def riseStateCallback(self, data):
        rise_state_event = json.loads(data.data)["transition_event"]
        rise_state_state = json.loads(data.data)["current_state"]

        self.inputTextField.config(state='normal')
        self.outputTextField.config(state='normal')
        if(rise_state_event == self.updateInputFieldIRState):
            print("get asr value")
            self.inputTextField.delete('1.0',END)
            self.outputTextField.delete('1.0',END)
            asrValue = self.getValue("H_asr_request")
            self.inputTextField.insert(1.0, asrValue)
        elif(rise_state_event == self.updateOutputFieldIRState):
            self.outputTextField.delete('1.0',END)
            gptValue = self.getValue("R_gpt_response")
            self.outputTextField.insert(1.0, gptValue)
        elif(rise_state_state == self.waitForRequestIRState):
            self.pushToTalkButton.config(state='normal')
            self.pushToTalkButton.config(text="Press to start listening", background=self.recordingNotActivColor,activebackground=self.recordingNotActivColor)
        self.inputTextField.config(state='disabled')
        self.outputTextField.config(state='disabled')
        

    def pushToTalkButton_Pressed(self):
        global asrIsActiv
        boolMessage = Bool()
        
        if(self.asrIsActiv):
            self.pushToTalkButton.config(state='disabled')
            boolMessage.data=FALSE
            self.asrIsActiv=False
            self.pushToTalkButton.config(text="Generating..",background=self.isGeneratingColor,activebackground=self.isGeneratingColor)

        else:
            boolMessage.data=TRUE
            self.asrIsActiv=True
            self.pushToTalkButton.config(text="Press to stop listening",background=self.recordingActivColor,activebackground=self.recordingActivColor)
        self.asrControllingPublisher.publish(boolMessage)

        
    def getValue(self,key):
        rospy.wait_for_service('/rise/set/memoryService')
        try:
            memoryService = rospy.ServiceProxy('/rise/set/memoryService', WorkingMemoryService)
            resp1 = memoryService(key)
            pair = json.loads(resp1.json_response)
            return pair[key]
        except rospy.ServiceException as e:
            print("Service call failed: %s"%e)

    def resetButton_Pressed(self):
        self.controllPublisher.publish("button1 pressed")
    
if __name__ == "__main__":
    app = IoGuiApp("IO_GUI")
    app.run()